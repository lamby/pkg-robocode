/*******************************************************************************
 * Copyright (c) 2001, 2008 Mathew A. Nelson and Robocode contributors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://robocode.sourceforge.net/license/cpl-v10.html
 *
 * Contributors:
 *     Pavel Savara
 *     - Initial implementation
 *******************************************************************************/
package robocode.manager;


import robocode.control.snapshot.IRobotSnapshot;
import robocode.dialog.BattleButton;
import robocode.dialog.BattleDialog;
import robocode.dialog.RobotButton;
import robocode.dialog.RobotDialog;

import java.util.List;


/**
 * @author Pavel Savara (original)
 */
public interface IRobotDialogManager {
	void trim(List<IRobotSnapshot> robots);

	void reset();

	RobotDialog getRobotDialog(RobotButton robotButton, String name, boolean create);
	BattleDialog getBattleDialog(BattleButton battleButton, boolean create);
}
